﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RFS
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			WebView webView = new WebView();
			UrlWebViewSource urlSource = new UrlWebViewSource();
			urlSource.Url = System.IO.Path.Combine(DependencyService.Get<IBaseUrl>().Get(), "index.html");			
			webView.Source = urlSource;
			this.Content = webView;
		}
	}

	public interface IBaseUrl { string Get(); }
}
